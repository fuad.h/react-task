import styled from "styled-components";

export const Container = (props) => {
  const { children } = props;
  return <Wrapper>{children}</Wrapper>;
};

const Wrapper = styled.div`
  padding: 22px;
`;
