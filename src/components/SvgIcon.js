import { useState } from "react";
import styled from "styled-components";

export const SvgIcon = (props) => {
  const {
    width,
    height,
    fill,
    viewBox,
    paths,
    onHoverColor,
    className,
    ...rest
  } = props;

  const [filledColor, setFilledColor] = useState(paths[0].fill);

  return (
    <Svg
      className={className}
      width={width}
      height={height}
      viewBox={viewBox}
      fill={fill}
      {...rest}
      onMouseEnter={() =>
        setFilledColor((color) => (onHoverColor ? onHoverColor : color))
      }
      onMouseLeave={() => setFilledColor(paths[0].fill)}
    >
      {paths.map(({ fill, stroke, strokeWidth, d }, index) => (
        <path
          key={index}
          stroke={stroke}
          strokeWidth={strokeWidth}
          fill={filledColor}
          d={d}
        />
      ))}
    </Svg>
  );
};

const Svg = styled.svg`
  cursor: pointer;
`;
