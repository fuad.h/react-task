import styled from "styled-components";

import { useState, useEffect } from "react";

import { Search } from "./Search";
import { Row } from "./Row";
import { TextColor } from "../../styles/styles";
import { SvgIcon } from "../../components/SvgIcon";
import { RESET } from "../../assets/reset";

export const Header = (props) => {
  const { columnName, setSearched, initials, employees, resetEmployees } =
    props;
  const [mayReset, setMayReset] = useState(false);

  useEffect(() => {
    setMayReset(employees.find((employee) => employee.status !== "Original"));
  }, [employees]);

  const color = mayReset ? "#0A65FF" : "#808080";

  const resetHandler = () => {
    if (mayReset) {
      resetEmployees(initials);
    }
  };

  return (
    <Container>
      <Wrapper>
        <Search onChange={(e) => setSearched(e.target.value)} />
        <Button color={color} onClick={resetHandler}>
          <SvgIcon {...RESET} onHoverColor={color} />
          <Text>RESET ALL CHANGES</Text>
        </Button>
      </Wrapper>
      <Row>
        {columnName.map((name) => (
          <Column key={name} color={TextColor}>
            {name}
          </Column>
        ))}
      </Row>
    </Container>
  );
};

const Container = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  flex-direction: column;
  background-color: #f4f7fc;
  box-sizing: border-box;
  padding: 10px 10px;
`;

const Column = styled.div`
  width: 100%;
  margin: 10px 0;
  font-size: 13px;
  line-height: 10px;
  font-weight: 800;
  color: ${(props) => props.color};
  cursor: default;
`;

const Wrapper = styled.div`
  width: 100%;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  margin-bottom: 10px;
`;

const Button = styled.div`
  width: 200px;
  height: 40px;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  border-radius: 4px;
  padding: 0 12px;
  border: 1px solid ${(props) => props.color};
  cursor: pointer;
  color: ${(props) => props.color};
`;

const Text = styled.div`
  font-weight: 600px;
  font-size: 14px;
  line-height: 16px;
`;
