import styled from "styled-components";

export const Submit = (props) => {
  const { employees } = props;
  const updated = employees.filter((employee) => employee.status === "Updated");
  const deleted = employees.filter((employee) => employee.status === "Deleted");

  const onSubmitHandler = () => {
    console.log(JSON.stringify({ updated, deleted }));
  };
  return (
    <Container onClick={onSubmitHandler}>
      <Button>Submit</Button>
    </Container>
  );
};

const Container = styled.div`
  padding: 10px 10px;
  width: 100%;
  display: flex;
  justify-content: flex-end;
  cursor: pointer;
`;

const Button = styled.div`
  border-radius: 4px;
  background-color: #0a65ff;
  padding: 12px 24px;
  font-weight: 600;
  font-size: 14px;
  line-height: 16px;
  color: #fff;
`;
