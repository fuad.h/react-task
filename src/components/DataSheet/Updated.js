import styled from "styled-components";

export const Updated = () => {
  return (
    <Container>
      <Wrapper>Updated</Wrapper>
    </Container>
  );
};

const Container = styled.div`
  font-size: 14px;
  line-height: 16px;
  padding: 18px 0;
  background-color: transparent;
  word-wrap: break-word;
  white-space: pre-wrap;
  width: 100%;
`;

const Wrapper = styled.div`
  padding: 4px 9px;
  border-radius: 11px;
  background-color: #0a65ff;
  width: fit-content;
  color: #fff;
  font-size: 12px;
  line-height: 12px;
`;
