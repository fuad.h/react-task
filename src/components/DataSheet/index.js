import { useEffect, useRef } from "react";
import { connect } from "react-redux";
import { selectEmployees, setEmployees, resetEmployees } from "./store/data";

import { Connect } from "./Connect";

const mapStateToProps = (state) => ({
  employees: selectEmployees(state),
});

export const DataSheet = connect(mapStateToProps, {
  setEmployees,
  resetEmployees,
})((props) => {
  const { headers, data, setEmployees, employees, resetEmployees } = props;

  const initialDatas = useRef(data);

  useEffect(() => {
    setEmployees(data);
  }, []);

  return (
    <Connect
      headers={headers}
      employees={employees}
      initialDatas={initialDatas}
      resetEmployees={resetEmployees}
    />
  );
});
