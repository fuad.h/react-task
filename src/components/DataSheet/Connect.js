import { useEffect, useState } from "react";

import { Header } from "./Header";
import { Table } from "./Table";
import { Pagination } from "./Pagination";
import { Submit } from "./Submit";

export const Connect = (props) => {
  const { headers, employees, initialDatas, resetEmployees } = props;

  const [employeesList, setEmployeesList] = useState(employees);
  const [searched, setSearched] = useState("");

  const [currentPage, setCurrentPage] = useState(1);
  const [startIndex, setStartIndex] = useState(0);
  const [employeePerPage, setEmployeePerPage] = useState(10);

  useEffect(() => {
    setEmployeesList(
      employees.filter((employee) => {
        for (let key in employee) {
          if (employee[key].toLowerCase().includes(searched.trim()))
            return employee;
        }
      })
    );
  }, [employees, searched]);

  const currentEmployeesList = employeesList.slice(
    startIndex,
    startIndex + employeePerPage
  );

  const totalPageNumber = Number.isInteger(
    employeesList.length / employeePerPage
  )
    ? employeesList.length / employeePerPage
    : Math.trunc(employeesList.length / employeePerPage) + 1;

  return (
    <>
      <Header
        {...headers}
        setSearched={setSearched}
        initials={initialDatas.current}
        employees={employees}
        resetEmployees={resetEmployees}
      />
      <Table employees={currentEmployeesList} initialDatas={initialDatas} />
      <Pagination
        total={employeesList.length}
        totalPageNumber={totalPageNumber}
        currentPage={currentPage}
        setStartIndex={setStartIndex}
        employeePerPage={employeePerPage}
        setCurrentPage={setCurrentPage}
      />
      <Submit employees={employees} />
    </>
  );
};
