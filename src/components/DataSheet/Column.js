import { useState, useEffect } from "react";
import styled from "styled-components";

import { Cell } from "./Cell";
import { Row } from "./Row";
import { SvgIcon } from "../SvgIcon";

import { UNDO } from "../../assets/undo";
import { BIN } from "../../assets/bin";

import { ColumnBackgroundColor } from "../../styles/styles";

export const Column = (props) => {
  const {
    employee,
    showIcon,
    deleteEmployee,
    initial,
    updateEmployee,
    undoEmployee,
  } = props;
  const [column, setColumn] = useState(employee);
  const [updateable, setUpdateable] = useState(false);

  useEffect(() => {
    setColumn(employee);
  }, [employee]);

  const color = showIcon
    ? employee.status === "Original"
      ? "#fff"
      : ColumnBackgroundColor
    : "transparent";

  const columnChangeHandler = (e) => {
    const { name, value } = e.target;
    setColumn((column) => ({
      ...column,
      [name]: value,
    }));
  };

  const onEnterPressHandler = (key) => {
    if (updateable) {
      if (initial[key] !== column[key]) {
        if (column.status === "Original") {
          updateEmployee({ ...column, status: "Updated" });
        } else {
          updateEmployee(column);
        }
      } else {
        if (compareObject(initial, column, key)) {
          updateEmployee(initial);
        } else {
          updateEmployee(column);
        }
      }
    } else {
      alert("Make correction");
    }
  };

  const onUndoChange = () => {
    undoEmployee(initial);
  };

  return (
    <ColumnWrapper color={color}>
      <Row>
        {Object.keys(column).map((key, index) => {
          return (
            <Cell
              key={`${key}${index}`}
              index={index}
              value={column[key]}
              keyOfColumn={key}
              status={column.status}
              columnChangeHandler={columnChangeHandler}
              onEnterPressHandler={() => onEnterPressHandler(key)}
              setUpdateable={setUpdateable}
            />
          );
        })}
      </Row>

      <Icons>
        {employee.status !== "Original" && (
          <SvgIcon {...UNDO} onClick={onUndoChange} />
        )}
        {employee.status !== "Deleted" && (
          <StyledSvgIcon
            {...BIN}
            onClick={() => {
              if (updateable) deleteEmployee(column);
            }}
          />
        )}
      </Icons>
    </ColumnWrapper>
  );
};

const ColumnWrapper = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  background-color: ${(props) => props.color};
`;

const Icons = styled.div`
  width: calc(7% - 20px);
  display: flex;
  flex-direction: row;
  justify-content: flex-end;
`;

const StyledSvgIcon = styled(SvgIcon)`
  margin-left: 10px;
`;

function compareObject(obj1, obj2, objKey) {
  for (let key in obj1) {
    if (key !== objKey && key !== "status") {
      if (obj1[key] !== obj2[key]) {
        return false;
      }
    }
  }

  return true;
}
