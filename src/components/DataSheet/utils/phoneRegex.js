export function checkValidPhoneFormat(phone) {
  const validRegex = /^[\+]?[0-9]{12,}$/im;
  return phone.match(validRegex);
}
