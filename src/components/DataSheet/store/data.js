export const MODULE_NAME = "data";

export const selectEmployees = (state) => state[MODULE_NAME].employees;

const SET_EMPLOYEES = "SET_EMPLOYEES";
const DELETE_EMPLOYEE = "DELETE_EMPLOYEE";
const UPDATE_EMPLOYEE = "UPDATE_EMPLOYEE";
const UNDO_EMPLOYEE = "UNDO_EMPLOYEE";
const RESET_EMPLOYEES = "RESET_EMPLOYEES";

const initialState = {
  employees: [],
};

export function reducer(state = initialState, { type, payload }) {
  switch (type) {
    case SET_EMPLOYEES:
      return {
        ...state,
        employees: payload,
      };

    case RESET_EMPLOYEES:
      return {
        ...state,
        employees: payload,
      };

    case DELETE_EMPLOYEE:
      return {
        ...state,
        employees: state.employees.map((employee) => {
          if (employee.id === payload.id)
            return { ...payload, status: "Deleted" };
          return employee;
        }),
      };

    case UPDATE_EMPLOYEE:
      return {
        ...state,
        employees: state.employees.map((employee) => {
          if (employee.id === payload.id) return payload;
          return employee;
        }),
      };

    case UNDO_EMPLOYEE:
      return {
        ...state,
        employees: state.employees.map((employee) => {
          if (employee.id === payload.id) return payload;
          return employee;
        }),
      };
    default:
      return state;
  }
}

export const setEmployeesAction = (payload) => ({
  type: SET_EMPLOYEES,
  payload,
});

const resetEmployeesAction = (payload) => ({
  type: RESET_EMPLOYEES,
  payload,
});

export const deleteEmployeeAction = (payload) => ({
  type: DELETE_EMPLOYEE,
  payload,
});

export const updateEmployeeAction = (payload) => ({
  type: UPDATE_EMPLOYEE,
  payload,
});

export const undoEmployeeAction = (payload) => ({
  type: UNDO_EMPLOYEE,
  payload,
});

export const setEmployees = (payload) => (dispatch) => {
  dispatch(setEmployeesAction(payload));
};

export const resetEmployees = (payload) => (dispatch) => {
  dispatch(resetEmployeesAction(payload));
};

export const deleteEmployee = (payload) => (dispatch) => {
  dispatch(deleteEmployeeAction(payload));
};

export const updateEmployee = (payload) => (dispatch) => {
  dispatch(updateEmployeeAction(payload));
};

export const undoEmployee = (payload) => (dispatch) => {
  dispatch(undoEmployeeAction(payload));
};
