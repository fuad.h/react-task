import styled from "styled-components";

export const Row = (props) => {
  const { children, className } = props;
  return <Container className={className}>{children}</Container>;
};

const Container = styled.div`
  width: calc(100% - 8%);
  display: flex;
  flex-direction: row;
  margin: 0 10px;
`;
