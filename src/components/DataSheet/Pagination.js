import styled from "styled-components";

import { SvgIcon } from "../SvgIcon";
import { LEFT } from "../../assets/left";
import { RIGHT } from "../../assets/right";

export const Pagination = (props) => {
  const {
    total,
    totalPageNumber,
    currentPage,
    setStartIndex,
    employeePerPage,
    setCurrentPage,
  } = props;

  const increementPage = () => {
    setStartIndex((index) => index + employeePerPage);
    setCurrentPage((page) => page + 1);
  };
  const decreementPage = () => {
    setStartIndex((index) => index - employeePerPage);
    setCurrentPage((page) => page - 1);
  };

  return (
    <Container>
      <TotalCount>TOTAL COUNT OF EMPLOYEE : {total}</TotalCount>
      <IconWrapper>
        <SvgIcon {...LEFT} onClick={decreementPage} />
        <TotalCount>{currentPage}</TotalCount>
        <SvgIcon {...RIGHT} onClick={increementPage} />
        <TotalCount>of {totalPageNumber == 0 ? 1 : totalPageNumber}</TotalCount>
      </IconWrapper>
    </Container>
  );
};

const Container = styled.div`
  padding: 10px;
  width: 100%;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  margin-top: 20px;
`;

const TotalCount = styled.div`
  font-weight: 500;
  font-size: 10px;
  line-height: 12px;
  color: #606f89;
`;

const IconWrapper = styled.div`
  width: 5%;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  margin-right: 50px;
`;
