import { useEffect, useState } from "react";

import styled from "styled-components";
import { TextColor } from "../../styles/styles";
import { Updated } from "./Updated";
import { Deleted } from "./Deleted";
import { checkValidPhoneFormat } from "./utils/phoneRegex";
import { checkBirthDateFormat } from "./utils/birthdayRegex";

export const Cell = (props) => {
  const {
    value,
    index,
    status,
    columnChangeHandler,
    keyOfColumn,
    onEnterPressHandler,
    setUpdateable,
  } = props;

  const [valid, setValid] = useState(true);

  useEffect(() => {
    if (keyOfColumn === "phone") {
      if (!checkValidPhoneFormat(value)) {
        setValid(false);
        setUpdateable(false);
      } else {
        setValid(true);
        setUpdateable(true);
      }
    }
    if (keyOfColumn === "dateOfBirth") {
      if (!checkBirthDateFormat(value)) {
        setValid(false);
        setUpdateable(false);
      } else {
        setValid(true);
        setUpdateable(true);
      }
    }
  }, [value]);

  if (value === "Updated") return <Updated />;
  if (value === "Deleted") return <Deleted />;

  const disabled =
    status === "Deleted" ? true : index === 0 || index === 6 ? true : false;

  return (
    <Container color={TextColor}>
      <StyledInput
        disabled={disabled}
        autoComplete="off"
        value={value}
        name={keyOfColumn}
        color={valid ? TextColor : "red"}
        onChange={(e) => columnChangeHandler(e)}
        onKeyPress={(e) => {
          if (e.code === "Enter") {
            onEnterPressHandler();
          }
        }}
      />
      {!valid && (
        <FailText>
          {keyOfColumn === "phone" ? "+xxxxxxxxxx" : "dd.mm.yyy"}
        </FailText>
      )}
    </Container>
  );
};

const Container = styled.div`
  font-size: 14px;
  line-height: 16px;
  padding: 18px 0;
  background-color: transparent;
  word-wrap: break-word;
  white-space: pre-wrap;
  width: 100%;
  color: ${(props) => props.color};
  position: relative;
`;

const StyledInput = styled.input`
  font-size: 14px;
  line-height: 16px;
  color: ${(props) => props.color};
  border: none;
  background-color: transparent;
  outline: none;
  width: 100%;
  height: 100%;
`;

const FailText = styled.div`
  position: absolute;
  bottom: 2px;
  color: red;
  font-size: 10px;
  left: 4px;
`;
