import styled from "styled-components";

export const Deleted = () => {
  return (
    <Container>
      <Wrapper>Deleted</Wrapper>
    </Container>
  );
};

const Container = styled.div`
  font-size: 14px;
  line-height: 16px;
  padding: 18px 0;
  background-color: transparent;
  word-wrap: break-word;
  white-space: pre-wrap;
  width: 100%;
`;

const Wrapper = styled.div`
  padding: 4px 9px;
  border-radius: 11px;
  background-color: transparent;
  width: fit-content;
  border: 1px solid #606f89;
  color: #606f89;
  font-size: 12px;
  line-height: 12px;
`;
