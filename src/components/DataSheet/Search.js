import styled from "styled-components";

import { SvgIcon } from "../SvgIcon";
import { SEARCH } from "../../assets/search";
import { InputBackgroundColor } from "../../styles/styles";

export const Search = (props) => {
  const { className, ...rest } = props;
  return (
    <Container color={InputBackgroundColor} className={className}>
      <SvgIcon {...SEARCH} />
      <StyledInput
        placeholder="Search"
        color={InputBackgroundColor}
        {...rest}
      />
    </Container>
  );
};

const Container = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  background-color: ${(props) => props.color};
  border-radius: 4px;
  width: 24%;
  padding: 10px;
  box-sizing: border-box;
`;

const StyledInput = styled.input`
  border: none;
  outline: none;
  font-size: 14px;
  line-height: 16px;
  font-weight: 400;
  margin-left: 10px;
  background-color: ${(props) => props.color};
  width: 100%;
`;
