import { useRef, useState } from "react";

import { connect } from "react-redux";

import { Column } from "./Column";

import { deleteEmployee, updateEmployee, undoEmployee } from "./store/data";

export const Table = connect(null, {
  deleteEmployee,
  updateEmployee,
  undoEmployee,
})((props) => {
  const {
    employees,
    deleteEmployee,
    updateEmployee,
    undoEmployee,
    initialDatas,
  } = props;

  return (
    <>
      {employees.map((employee, index) => (
        <Column
          key={index}
          employee={employee}
          showIcon="true"
          index={index}
          deleteEmployee={deleteEmployee}
          initial={initialDatas.current[index]}
          updateEmployee={updateEmployee}
          undoEmployee={undoEmployee}
        />
      ))}
    </>
  );
});
